/// <reference path="./../../typings/browser">

import { platformBrowserDynamic } from "@angular/platform-browser-dynamic";
import { SmsModule } from "./app.module";
const platform = platformBrowserDynamic();
platform.bootstrapModule(SmsModule);