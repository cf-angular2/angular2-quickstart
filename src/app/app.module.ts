import { NgModule }      from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { DashboardComponent } from "./components/dashboard/index";

@NgModule({
  imports: [BrowserModule],
  declarations: [DashboardComponent],
  bootstrap: [DashboardComponent]
})
export class SmsModule {
}