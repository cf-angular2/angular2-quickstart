const gulp        = require("gulp");
const tsc         = require("gulp-typescript");
const tslint      = require("gulp-tslint");
const uglify      = require("gulp-uglify");
const concat      = require("gulp-concat");
const cssmin      = require("gulp-cssmin");
const rename      = require("gulp-rename");
const minifyHTML  = require("gulp-minify-html");
const gulpCopy    = require("gulp-copy");
const wiredep     = require("wiredep").stream;
const inject      = require("gulp-inject");
const bowerFiles  = require("main-bower-files");
const browserSync = require("browser-sync").create();
const watch       = require("gulp-watch");
const sass        = require("gulp-sass");
const eslint      = require("gulp-eslint");
const sourcemaps  = require("gulp-sourcemaps");
const del         = require("del");

const config    = require("./gulp.config")();
const tsProject = tsc.createProject("./tsconfig.json");

/*
 Development.
 -------------------------------------------------------------------------------
 */

/**
 * Remove public directory.
 */
gulp.task("clean", (cb) => {
  return del([config.build.path], cb);
});

/**
 * Copy all required libraries into build directory.
 */
gulp.task("libs", () => {
  return gulp
    .src(config.libs.src, {cwd: config.libs.cwd})
    .pipe(gulp.dest(config.libs.dest));
});

/**
 * Copy all resources that are not TypeScript files into build directory.
 */
gulp.task("resources", () => {
  return gulp
    .src(config.resources.src)
    .pipe(gulp.dest(config.resources.dest));
});

/**
 * Minify html.
 */
gulp.task("htmlmin", () => {
  return gulp
    .src(config.htmlmin.src)
    .pipe(minifyHTML(config.htmlmin.options))
    .pipe(gulp.dest(config.htmlmin.dest));
});

/**
 * Compile TypeScript sources and create sourcemaps in public directory.
 */
gulp.task("compile", () => {
  var tsResult = gulp
    .src(config.compile.src)
    .pipe(sourcemaps.init())
    .pipe(tsc(tsProject));

  return tsResult
    .js
    .pipe(sourcemaps.write(config.compile.sourcemaps.write))
    .pipe(gulp.dest(config.compile.dest));
});

/**
 * Semantic and syntactic TypeScript checks.
 */
gulp.task("tslint", () => {
  return gulp
    .src(config.tslint.src)
    .pipe(tslint(config.tslint.options))
    .pipe(tslint.report(config.tslint.reporter));
});

/**
 * Compile SASS sources.
 * https://www.npmjs.com/package/gulp-sass
 */
gulp.task("sass", () => {
  return gulp
    .src(config.sass.src)
    .pipe(sass().on("error", sass.logError))
    .pipe(rename({suffix: ".min"}))
    .pipe(gulp.dest(config.sass.dest));
});

/**
 * Minify css.
 */
gulp.task("cssmin", ["sass"], () => {
  return gulp
    .src(config.cssmin.src)
    .pipe(cssmin())
    .pipe(gulp.dest(config.cssmin.dest));
});

/**
 * Prepare Browser-sync for localhost.
 */
gulp.task("browserSync", () => {
  browserSync.init(config.browserSync.init);
});

/**
 * Watch for changes in TypeScript, HTML and SCSS files.
 */
gulp.task("watch", ["browserSync"], () => {
  // Watch TypeScripts files.
  gulp.watch(config.watch.ts.src, ["compile"])
      .on("change", (e) => {
        console.log(`TypeScript file ${e.path} has been changed. Compiling.`);
      });

  // Watch Resources files.
  gulp.watch(config.watch.html.src, ["resources"])
      .on("change", (e) => {
        console.log(`Resource file ${e.path} has been changed. Updating.`);
      });

  // Watch SASS files.
  gulp.watch(config.watch.scss.src, ["sass"])
      .on("change", (e) => {
        console.log(`SASS file ${e.path} has been changed. Updating.`);
      });
});

/*
 Production.
 -------------------------------------------------------------------------------
 */

/**
 * Build the project.
 */
gulp.task("build", ["compile", "resources", "cssmin", "htmlmin", "libs"], () => {
  del(["public/scss"]);
  console.log("Building the project ...");
});