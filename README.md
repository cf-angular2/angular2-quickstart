# CF: angular2-quickstart

This sekeleton project is based on the Angular2 Quickstart: https://angular.io/docs/ts/latest/quickstart.html with some changes and gulp tasks. Our version to quickly bootstrap an angular 2 webapp projects.

[TOC]

## Project layout

- Separate source files from build files.
	- **src** directory contains all the source files.
	- **public** directory contains all compiled and processed files.

```
.
├── README.md
├── src								// Folder with source code.
│   ├── app							// Example Angular2 module but can have others.
│   │   ├── components
│   │   │   └── dashboard			// Component dashboard.
│   │   │       └── index.ts
│   │   ├── app.module.ts			// App module definition.
│   │   └── main.ts                 // App bootstrap.
│   ├── scss						// Folder with SASS code files.
|   ├── index.html                  // Starting point for the app.
│   └── systemjs.config.js          // SystemJS configuration.
├── gulp.config.js                  // Gulp configuration.
├── gulpfile.js                     // Gulp tasks.
├── tsconfig.json                   // Typescript configuration.
└── typings.json                    // Typescript typings definitions.
```

## Gulp taks

- `gulp clean` Remove **public** folder.
- `gulp libs` Copy all required libraries into **public** directory.
- `gulp resources` Copy all resources that are not TypeScript files into **public** directory.
- `gulp htmlmin` Minify html.
- `gulp compile` Compile TypeScript sources and create sourcemaps into **public** directory.
- `gulp tslint` Semantic and syntactic TypeScript checks.
- `gulp sass` Compile SASS sources and copy into **public** directory.
- `gulp cssmin` Minify css in **public** directory.
- `gulp browserSync` Prepare Browser-sync for localhost.
- `gulp watch` Watch for changes in TypeScript, HTML and SCSS files.
- `gulp build` Build the project.

## NPM tasks

Each Gulp task can be called from npm, example: `npm run build`.

There is also the `npm run start` with the following tasks are executed in order: `npm run clean`, `npm run build` y `npm run watch`

## Prerequisites
- Install nodejs: [https://nodejs.org/download/](https://nodejs.org/download/)
- Install typescript:
`npm install -g typescript`
- Install gulp:
`npm install -g gulp`

## How to install

- Clone this project from Gitlab:
`git clone https://gitlab.com/cf-angular2/angular2-quickstart.git; cd angular2-quickstart;`
- Install the packages listed in **package.json** with the command:
`npm install`
- Building and running:
`npm run start`
- Open browser: http://localhost:3000