const modRewrite = require("connect-modrewrite");

module.exports = function () {
  const root  = "";
  const src   = root + "src";
  const index = src + "/index.html";
  const cwd   = root + "node_modules/**";

  const build = {
    path: root + "public"
  };

  const browserSync = {
    init: {
      port   : 3000,
      browser: "google-chrome",
      notify : true,
      "files": [
        `${build.path}/**/*.{html,htm,css,js}`
      ],
      server : {
        baseDir   : build.path,
        middleware: [
          modRewrite([
            '!\\.\\w+$ /index.html [L]'
          ])
        ]
      }
    }
  };

  const watch = {
    ts  : {
      src: [
        `${src}/**/*.ts`
      ]
    },
    html: {
      src: [
        `${src}/**/*.html`
      ]
    },
    scss: {
      src: [
        `${src}/**/*.scss`
      ]

    }
  };

  const libs = {
    src : [
      "systemjs/dist/system-polyfills.js",
      "core-js/client/shim.min.js",
      "systemjs/dist/system.src.js",
      "reflect-metadata/Reflect.js",
      "rxjs/**",
      "zone.js/dist/**",
      "@angular/**"
    ],
    cwd : cwd,
    dest: build.path + "/libs"
  };

  const resources = {
    src : [
      `${src}/**/*`,
      `!${src}/**/*.ts`,
      `!${src}/**/*.scss`
    ],
    dest: build.path
  };

  const htmlmin = {
    src    : [
      `${src}/**/*.html`
    ],
    options: {
      quotes: true
    },
    dest   : build.path
  };

  const compile = {
    src       : `${src}/**/*.ts`,
    sourcemaps: {
      write: "."
    },
    dest      : build.path
  };

  const tslint = {
    src     : `${src}/**/*.ts`,
    options : {
      "configuration": {
        "rules": {
          "class-name": true
        }
      }
    },
    reporter: "verbose"
  };

  const sass = {
    src : `${src}/scss/**/*.scss`,
    dest: build.path
  };

  const cssmin = {
    src : [
      `${build.path}/**/*.css`
    ],
    dest: build.path
  };

  const config = {
    build      : build,
    browserSync: browserSync,
    watch      : watch,
    libs       : libs,
    resources  : resources,
    htmlmin    : htmlmin,
    compile    : compile,
    tslint     : tslint,
    sass       : sass,
    cssmin     : cssmin
  };

  return config;
};